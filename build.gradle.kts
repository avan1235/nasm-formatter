plugins {
    application
    kotlin("jvm") version "1.3.61" 
}

repositories {
    jcenter() 
}

val MAIN_CLASS = "com.procyk.maciej.FormatterKt"

application {
    mainClassName = MAIN_CLASS
}

dependencies {
    implementation(kotlin("stdlib")) 
}

tasks.withType<Jar>() {
    manifest {
        attributes["Main-Class"] = MAIN_CLASS
    }
    configurations["compileClasspath"].forEach { file ->
        from(zipTree(file.absoluteFile))
    }
}

